## Repear

A CLI to re-pair a group of developers

### Getting Started

1. Install `repear`:

    ```bash
    git clone git@gitlab.com:jpsamper/repear
    cd repear
    ./setup
    ```
    - **Note:** Make sure that `~/.local/bin` is in your `PATH`

2. Create a repository for `repear` to track pairs:

    ```bash
    mkdir -p ~/repear-repo
    cd ~/repear-repo
    repear add 'John Doe'  # Any developers name
    A repear repository was not found. Would you like to create one in <cwd>? [y/N]: y
    Hello John Doe!
    ...

    Shall we update the matrix? [Y/n]: y
    ```

3. Add more developers:

    ```bash
    repear --workdir ~/repear-repo add 'Jane Doe'
    ```
    - **Note:** Currently, only one developer can be added at a time

4. Remove developers:

    ```bash
    repear --workdir ~/repear-repo remove 'Jane Doe'
    ```

5. Generate a new set of pairs:

    ```bash
    repear --workdir ~/repear-repo repair
    ```
    - Anchors and unavailable developers must be specified as a comma-separated list
    - The command will print the list of pairs
    - To see the latest pairs, `cat PairState.csv` in the repear repo
    - To see the number of time each pair of developers has worked together,
      `cat PairMatrix.csv`

### Developing

1. Install repear with the develop option:

    ```bash
    git clone git@gitlab.com:jpsamper/repear
    cd repear
    ./setup develop
    source venv/bin/activate
    ```
    - **Note:** Repear is installed in such a way that edits to the code can be run directly without having to reinstall

2. Running tests:

    ```bash
    $ flake8  # Checks for style
    $ pytest -m pylint --pylint  # Lints python code
    $ pytest --cov=repear --cov-branch  # Runs all tests and outputs coverage
    ```
