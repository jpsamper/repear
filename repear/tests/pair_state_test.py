"""Tests for PairMatrix Class"""

import os
import shutil
import pytest


from repear.pair_state import PairState
from repear.cli import PAIR_STATE_FILENAME


@pytest.fixture(name='pair_state')
def load_ps(tmpdir):
    """Test fixture to create the PairState"""
    cwd = os.path.abspath(os.path.dirname(__file__))
    ps_abspath = os.path.join(cwd, PAIR_STATE_FILENAME)
    shutil.copy(ps_abspath, tmpdir)
    tmp_ps_path = os.path.join(tmpdir, PAIR_STATE_FILENAME)
    return PairState(tmp_ps_path)


def test_init(pair_state):
    """Test __init__ function"""
    assert os.path.basename(pair_state.filepath) == PAIR_STATE_FILENAME
    assert len(pair_state.devs) == 4
    assert len(pair_state.pairs) == 2
    assert pair_state.pair_map['foo'] == 'bar'
    assert pair_state.pair_map['bar'] == 'foo'
    assert pair_state.pair_map['baz'] == 'qux'
    assert pair_state.pair_map['qux'] == 'baz'


def test_str(pair_state):
    """Test __str__ function"""
    expected = "[['foo', 'bar'], ['baz', 'qux']]"
    assert str(pair_state) == expected


def test_update(pair_state):
    """Test update function"""
    updated_pairs = [['foo', 'qux'], ['baz', 'bar']]

    pair_state.update(updated_pairs)
    read_state = PairState(pair_state.filepath)

    assert str(updated_pairs) == str(read_state)
