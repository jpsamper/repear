"""Test for the Repare CLI"""

import os
import shutil
from datetime import date
import pytest
from click.testing import CliRunner
from repear.cli import repear, PAIR_STATE_FILENAME, PAIR_MATRIX_FILENAME
from repear.pair_matrix import DuplicateName
from repear.utils import run, runout, is_valid_repear_repo

LARGE_PAIR_MATRIX_FILENAME = 'PairMatrixLarge.csv'


def _create_fixture(fixt_dir, pair_matrix_filename):
    cwd = os.path.abspath(os.path.dirname(__file__))
    pstate = os.path.join(cwd, PAIR_STATE_FILENAME)
    pmatrix = os.path.join(cwd, pair_matrix_filename)
    pmatrix_target = os.path.join(fixt_dir, PAIR_MATRIX_FILENAME)
    shutil.copy(pstate, fixt_dir)
    shutil.copy(pmatrix, pmatrix_target)
    run('git init'.split(), cwd=fixt_dir)
    run('git add *.csv'.split(), cwd=fixt_dir)
    run('git commit -am Init'.split(), cwd=fixt_dir)


@pytest.fixture(name='tmp_ws')
def create_tmp_ws(tmpdir):
    """Fixture for testing the CLI

    Creates a temporary git project with the PairMatrix
    and PairState CSV
    """
    _create_fixture(tmpdir, PAIR_MATRIX_FILENAME)
    return tmpdir


def test_help():
    """Test CLI help"""
    runner = CliRunner()
    result = runner.invoke(repear)
    assert result.exit_code == 0
    assert "Usage: " in result.output


def test_new_workdir(tmpdir):
    """Test repear repo initialization"""
    runner = CliRunner()
    # Default input
    result = runner.invoke(repear, ['--workdir', tmpdir, 'remove', 'John'], input='\n')
    assert "A repear repository has been created" not in result.output
    assert result.exit_code == 1
    # Explicit no
    result = runner.invoke(repear, ['--workdir', tmpdir, 'remove', 'John'], input='n\n')
    assert "A repear repository has been created" not in result.output
    assert result.exit_code == 1
    # Try changing directory
    cwd = os.path.abspath(os.curdir)
    os.chdir(tmpdir)
    result = runner.invoke(repear, ['remove', 'John'], input='\n')
    assert "A repear repository has been created" not in result.output
    assert result.exit_code == 1
    os.chdir(cwd)
    # Explicit yes
    result = runner.invoke(repear, ['--workdir', tmpdir, 'remove', 'John'], input='y\n')
    assert "A repear repository has been created" in result.output
    assert is_valid_repear_repo(tmpdir, PAIR_MATRIX_FILENAME, PAIR_STATE_FILENAME)
    assert result.exit_code == 0


def test_add(tmp_ws):
    """Test the 'add' CLI verb"""
    runner = CliRunner()
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'add'])
    assert result.exit_code == 2
    assert 'Error: Missing argument "NAME"' in result.output
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'add', 'foo'])
    assert isinstance(result.exception, DuplicateName)
    names = [
        "Jack",
        "John",
        "John Jacob",
        "John Jacob Jingleheimer Schmidt",
    ]
    name_row = 'name,foo,bar,baz,qux'
    pair_row = ',0,0,0,0'
    inputs = ['no', 'yes', 'n', 'y']
    for idx, name in enumerate(names):
        result = runner.invoke(repear, ['--workdir', tmp_ws, 'add', name], input=inputs[idx])
        assert result.exit_code == 0
        assert f'Hello {name}!' in result.output
        # Check that file was modified
        if inputs[idx] in ['no', 'n']:
            git_output = runout(f'git status'.split(), cwd=tmp_ws)
            assert f'{PAIR_MATRIX_FILENAME}' in git_output
        # Check that the new file has the list of names is updated
        result = runout(f'cat {PAIR_MATRIX_FILENAME}'.split(), cwd=tmp_ws)
        name_row += f',{name}'
        assert name_row in result
        # Check that a row has been added for the new name
        pair_row += ',0'
        assert name + pair_row in result


def test_remove(tmp_ws):
    """Test the 'remove' CLI verb"""
    runner = CliRunner()
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'remove'])
    assert result.exit_code == 2
    assert 'Error: Missing argument "NAME"' in result.output
    names = [
        "tat",  # Name doesn't exist
        "baz",  # Name in the middle
        "foo",  # Name at the beginning
        "qux",  # Name at the end
        "bar",  # All names
    ]
    name_row = 'name,foo,bar,baz,qux'
    inputs = ['_', 'y', 'no', 'yes', 'n']
    for idx, name in enumerate(names):
        result = runner.invoke(repear, ['--workdir', tmp_ws, 'remove', name], input=inputs[idx])
        assert result.exit_code == 0
        assert f'Goodbye {name}!' in result.output
        if name == 'tat':
            continue
        # Check that file was modified
        if inputs[idx] in ['n', 'no']:
            git_output = runout('git status'.split(), cwd=tmp_ws)
            assert f'{PAIR_MATRIX_FILENAME}' in git_output
        # Check that the new file has the list of names is updated
        result = runout(f'cat {PAIR_MATRIX_FILENAME}'.split(), cwd=tmp_ws)
        name_row = name_row.replace(f',{name}', '')
        assert name_row in result
        # Check that a row has been removed
        assert name not in result


def test_normal_repair(tmp_ws):
    """Test 'repair' CLI verb"""
    runner = CliRunner()
    inputs = '\n'          # unavailable prompt
    inputs += 'foo,baz\n'  # anchors prompt
    inputs += 'y\n'        # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "Another day, another dollar!" in result.output
    assert "[['foo', 'bar'], ['baz', 'qux']]" in result.output
    assert result.exit_code == 0
    git_output = runout('git log -1 --format=%s'.split(), cwd=tmp_ws)
    assert date.today().strftime('%Y-%m-%d') in git_output


def test_repair_no_commit(tmp_ws):
    """Test 'repair' with no commit"""
    runner = CliRunner()
    inputs = '\n'          # unavailable prompt
    inputs += 'foo,baz\n'  # anchors prompt
    inputs += 'n\n'        # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "We're stuck in yesteryear!" in result.output
    assert "[['foo', 'bar'], ['baz', 'qux']]" in result.output
    assert result.exit_code == 0


def test_repair_fixed_pairs(tmp_ws):
    """Test 'repair' with fixed pairs"""
    runner = CliRunner()
    inputs = '\n'                  # unavailable prompt
    inputs += 'foo,bar,baz,qux\n'  # anchors prompt
    inputs += 'yes\n'              # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "Another day, another dollar!" in result.output
    assert "[['foo', 'bar'], ['qux', 'baz']]" in result.output
    assert "foo and bar paired yesterday!" in result.output
    assert "qux and baz paired yesterday!" in result.output
    assert result.exit_code == 0


def test_repair_too_many_sidekicks(tmp_ws):
    """Test 'repair' with too many sidekicks"""
    runner = CliRunner()
    inputs = '\n'          # unavailable prompt
    inputs += 'foo,bar\n'  # anchors prompt
    inputs += 'yes\n'      # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "Pairing is not possible." in result.output
    assert "There are 2 sidekicks and 0 anchors!" in result.output
    assert result.exit_code == 1


def test_repair_odd_developers(tmp_ws):
    """Test 'repair' with an odd number of available developers"""
    runner = CliRunner()
    inputs = 'bar\n'       # unavailable prompt
    inputs += 'foo,baz\n'  # anchors prompt
    inputs += 'yes\n'      # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "The team has an odd number of developers available" in result.output
    assert result.exit_code == 1


def test_repair_unavailable(tmp_ws):
    """Test 'repair" with unavailable developers"""
    runner = CliRunner()
    inputs = 'baz,qux\n'    # unavailable prompt
    inputs += 'foo,bar\n'   # anchors prompt
    inputs += 'yes\n'       # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "Another day, another dollar!" in result.output
    assert "[['foo', 'bar']]" in result.output
    assert "foo and bar paired yesterday!" in result.output
    assert result.exit_code == 0


def test_repair_bad_anchor_input(tmp_ws):
    """Test 'repair" with bad anchor input"""
    runner = CliRunner()
    inputs = '\n'       # unavailable prompt
    inputs += 'Jack\n'  # anchors prompt
    inputs += 'yes\n'   # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "Jack is not on the team!" in result.output
    assert result.exit_code == 1


def test_repair_bad_unavailable_input(tmp_ws):
    """Test 'repair" with bad anchor input"""
    runner = CliRunner()
    inputs = 'bart\n'   # unavailable prompt
    inputs += 'foo\n'   # anchors prompt
    inputs += 'yes\n'   # commit prompt
    result = runner.invoke(repear, ['--workdir', tmp_ws, 'repair'], input=inputs)
    assert "bart is not on the team!" in result.output
    assert result.exit_code == 1


@pytest.fixture(name='large_tmp_ws')
def create_large_tmp_ws(tmpdir):
    """Fixture for testing the CLI

    Creates a temporary git project with the PairMatrix
    and PairState CSV
    """
    _create_fixture(tmpdir, LARGE_PAIR_MATRIX_FILENAME)
    return tmpdir


def test_repair_large_matrix(large_tmp_ws):
    """Test 'repair' with a larger matrix"""
    runner = CliRunner()
    inputs = 'fux\n'           # unavailable prompt
    inputs += 'foo,baz,tat\n'  # anchors prompt
    inputs += 'y\n'            # commit prompt
    result = runner.invoke(repear, ['--workdir', large_tmp_ws, 'repair'], input=inputs)
    assert "Another day, another dollar!" in result.output
    assert "[['foo', 'bar'], ['baz', 'qux'], ['tat', 'tut']]" in result.output
    assert result.exit_code == 0
    git_output = runout('git log -1 --format=%s'.split(), cwd=large_tmp_ws)
    assert date.today().strftime('%Y-%m-%d') in git_output
