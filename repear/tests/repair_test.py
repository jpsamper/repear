"""Tests for repair function"""

import os
import shutil
import pytest

from repear.pair_matrix import PairMatrix
from repear.repair import repair
from repear.cli import PAIR_MATRIX_FILENAME


@pytest.fixture(name='pair_matrix')
def load_pm(tmpdir):
    """Test fixture to create the PairMatrix"""
    # Run tests in the test directory
    cwd = os.path.abspath(os.path.dirname(__file__))
    pm_abspath = os.path.join(cwd, PAIR_MATRIX_FILENAME)
    shutil.copy(pm_abspath, tmpdir)
    tmp_pm_path = os.path.join(tmpdir, PAIR_MATRIX_FILENAME)
    return PairMatrix(tmp_pm_path)


def test_repair(pair_matrix):
    """Test basic repair"""
    anchors = ['foo', 'baz']
    sidekicks = set(['bar', 'qux'])
    pair_list = repair(anchors, sidekicks, pair_matrix)
    assert pair_list == [['foo', 'bar'], ['baz', 'qux']]

    anchors = ['qux', 'bar']
    sidekicks = set(['foo', 'baz'])
    pair_list = repair(anchors, sidekicks, pair_matrix)
    assert pair_list == [['bar', 'foo'], ['qux', 'baz']]

    anchors = ['bar', 'qux']
    sidekicks = set(['foo', 'baz'])
    pair_list = repair(anchors, sidekicks, pair_matrix)
    assert pair_list == [['bar', 'foo'], ['qux', 'baz']]
