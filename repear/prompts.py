"""Functions for prompting user"""

import sys
import click


def prompt_for_anchors(pair_matrix, pair_state):
    """Function to prompt for pair anchors"""
    anchors = click.prompt('Who are the anchors? (comma-separated, please)', type=str).split(',')
    fixed_pairs = []
    anchors = [dev.strip() for dev in anchors]
    for anchor in anchors:
        if anchor not in pair_matrix.get_devs():
            sys.exit(f'OH NO! {anchor} is not on the team!')
        if (anchor in pair_state.pair_map.keys()) and (pair_state.pair_map[anchor] in anchors):
            print(f'BTW, {anchor} and {pair_state.pair_map[anchor]} paired yesterday!')
            fixed_pairs.append([anchor, pair_state.pair_map[anchor]])
            anchors.remove(anchor)
            anchors.remove(pair_state.pair_map[anchor])
    return anchors, fixed_pairs


def prompt_for_unavailable(pair_matrix):
    """Function to prompt for unavailable devs"""
    prompt = 'Who is not available today? (comma-separated, please)'
    unavailable = click.prompt(prompt, type=str, default='')
    if unavailable.strip() == '':
        return []
    unavailable = unavailable.split(',')
    unavailable = [dev.strip() for dev in unavailable]
    for dev in unavailable:
        if dev not in pair_matrix.get_devs():
            print(f'WARNING: {dev} is not on the team!')
            unavailable.remove(dev)
    if len(pair_matrix.get_devs()) % 2 != len(unavailable) % 2:
        sys.exit('''
        UH OH! The team has an odd number of developers available.
        Try marking at least one more developer as unavailable.''')
    return unavailable


def validate_pairs(pairs):
    """Print pairs and prompt user"""
    validate_prompt = f'''
    Here are the new pairs!

    {pairs}

    Shall we update the matrix?'''
    return click.confirm(validate_prompt, default=True)


def validate_matrix(pair_matrix):
    """Print matrix and prompt user"""
    validate_prompt = f'''
    Here is the new matrix!

    {str(pair_matrix)}

    Shall we update the matrix?'''
    return click.confirm(validate_prompt, default=True)
