"""Functions for creating pairs"""

from collections import OrderedDict


def repair(anchors, sidekicks, pair_matrix):
    """Function to get new pairs from a pair_matrix"""
    # Copy pairing preferences to preserve original pair_matrix state
    sidekick_preferences = OrderedDict()
    for k, v in pair_matrix.data.items():
        sidekick_preferences[k] = v.copy()
    pairs = {}

    while len(sidekicks) > 0:
        sidekick = sidekicks.pop()
        first_anchor = ''
        # Make sure first_anchor is actually an anchor
        while first_anchor not in anchors:
            first_anchor = sidekick_preferences[sidekick].popitem(last=False)[0]
        # Stable Marriage Algorithm
        if first_anchor not in pairs.keys():
            pairs[first_anchor] = sidekick
        else:
            old_sidekick = pairs[first_anchor]
            old_sidekick_count = pair_matrix.data[first_anchor][old_sidekick]
            new_sidekick_count = pair_matrix.data[first_anchor][sidekick]
            if new_sidekick_count < old_sidekick_count:
                pairs[first_anchor] = sidekick
                sidekicks.add(old_sidekick)
            else:
                sidekicks.add(sidekick)
    # Convert pair dictionary to list of pairs
    pair_list = []
    for k, v in pairs.items():
        pair_list.append([k, v])
    return pair_list
